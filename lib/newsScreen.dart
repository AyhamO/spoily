import 'package:al_madar/network.dart';
import 'package:al_madar/widgets/Ticket.dart';
import 'package:flutter/material.dart';

class PlacesScreen extends StatefulWidget {
  @override
  PlacesScreenState createState() {
    return new PlacesScreenState();
  }
}

class PlacesScreenState extends State<PlacesScreen> {
  List<Widget> _places = [];

  @override
  void initState() {
    _getShops();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return _places[index];
      },
      itemCount: _places.length,
      padding: EdgeInsets.only(top: 8, bottom: 8),
    );
  }

  _getShops() {
    Network.getShops().then((placesList) {
      for (var i = 0; i < placesList.places.length; i++) {
        _places.add(Ticket(
          place: placesList.places[i],
        ));
      }
      setState(() {});
      print(placesList.places.first.name);
    });
  }
}
