import 'dart:async';
import 'dart:convert';
import 'package:al_madar/User.dart';
import 'package:al_madar/place.dart';
import 'package:http/http.dart' as http;

class Network {

  static Future<User> login(String email, String password) async {
    Map<String, String> body = {
      'email': email,
      'password': password,
    };
    final response = await http.post('http://shops.ngis-sa.com/api/v1/login', body: body);
    if (response.statusCode == 200) {
      return User.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load');
    }
  }

  static Future<User> register(String name, String email, String password) async {
    Map<String, String> body = {
      'name': name,
      'email': email,
      'password': password,
    };
    final response = await http.post('http://shops.ngis-sa.com/api/v1/register', body: body);
    if (response.statusCode == 200) {
      return User.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load');
    }
  }

  static Future<PlacesList> getShops() async {
    final response = await http.get('http://shops.ngis-sa.com/api/v1/shops',);
    if (response.statusCode == 200) {
      return PlacesList.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load');
    }
  }
}