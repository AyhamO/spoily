import 'package:al_madar/newsScreen.dart';
import 'package:al_madar/offersScreen.dart';
import 'package:al_madar/widgets/offer.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [

              new Tab(
                text: 'Places',
              ),
              new Tab(
                text: 'Offers',
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            PlacesScreen(),
            OffersScreen(),
          ],
        ),
      ),
    );
  }
}
