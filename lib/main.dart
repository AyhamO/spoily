import 'package:al_madar/detailsPage.dart';
import 'package:al_madar/mainScreen.dart';
import 'package:al_madar/registration/forgetPasswordScreen.dart';
import 'package:al_madar/registration/registrationScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
//      home: MyHomePage(title: 'Flutter Demo Home Page'),
    home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      theme: ThemeData(
        primaryColor: Colors.red,
        accentColor: Colors.redAccent,
        primaryColorDark: Colors.red[700],
      ),
      routes: <String, WidgetBuilder>{
        '/': (BuildContext context) => new LandingPage(),
        '/registrationScreen': (BuildContext context) =>
            new RegistrationScreen(),
        '/forgetPasswordScreen': (BuildContext context) =>
            new ForgetPasswordScreen(),
        '/mainScreen': (BuildContext context) => new MainScreen(),
        '/detailsScreen': (BuildContext context) => new DetailsPage(),
      },
    );
  }
}

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SharedPreferences.getInstance().then((prefs) {
      if (prefs.get("token") != null) {
        Navigator.pushReplacementNamed(context, '/mainScreen');
      }
      else {
        Navigator.pushReplacementNamed(context, '/registrationScreen');
      }
    });

    return Container(
      color: Colors.transparent,
    );
  }
}
