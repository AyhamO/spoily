class PlacesList {
  List<Place> places;

  PlacesList(this.places);

  factory PlacesList.fromJson(Map<String, dynamic> json) {
    final list = json['data'] as List;

    return PlacesList(
        list.map((jsonPlace) => Place.fromJson(jsonPlace)).toList()
    );
  }
}

class Place {

  int id;
  String name;
  String address;
  String number;
  int rating;
  Place(this.id, this.name, this.address, this.number);

  factory Place.fromJson(Map<String, dynamic> json) {
    return Place (
      json['id'],
      json['name'],
      json['address'],
      json['number'],
    );
  }


}