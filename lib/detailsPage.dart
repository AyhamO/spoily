import 'package:al_madar/place.dart';
import 'package:al_madar/widgets/arcBanner.dart';
import 'package:al_madar/widgets/ratingInformation.dart';
import 'package:flutter/material.dart';

class DetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            ArcBannerImage('assets/images/istanbul.png'),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                'Awesome place',
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            Center(child: RatingInformation(null)),
          ],
        ),
      ),
    );
  }
}
