import 'package:al_madar/widgets/offer.dart';
import 'package:flutter/material.dart';

class OffersScreen extends StatelessWidget {

  final offers = [
    Offer(),
    Offer(),
    Offer(),
    Offer(),
    Offer(),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return offers[index];
      },
      itemCount: offers.length,
      padding: EdgeInsets.only(top: 8, bottom: 8),
    );;
  }

}