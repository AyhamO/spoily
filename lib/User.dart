class User {
  String token;
  String name;
  String email;

  User(this.token, this.name, this.email);

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      json['token'],
      json['name'],
      json['email'],
    );
  }
}
